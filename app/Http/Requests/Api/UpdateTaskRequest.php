<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\TaskRequest;
class UpdateTaskRequest extends TaskRequest
{
    use ApiRequestTrait;   
}
